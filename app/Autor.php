<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    protected $fillable = [
    	'nombre', 'nacionalidad', 'bio',
    ];

    public function libros () {
    	$this->belongsToMany('App\Libro', 'autores_libros');
    }
}
